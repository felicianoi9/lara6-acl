<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    
    Route::group(['prefix'=>'painel'], function(){

        Route::get('/', 'Painel\PainelController@index')->name('painel');

    });

});



Route::get('/', function () {
    return "opa";
});


Route::get('/logout', function () {
    return "opa";
})->name('home');

Auth::routes();
